# position-specific-scoring-matrix

The code in this repository reimplements in an efficient way the algorithms described in

+ "Substrate specificity of the DnaK chaperone determined by screening cellulose-bound peptide libraries" by Stefan Rüdiger et al, 1997
+ "ChaperISM: improved chaperone binding prediction using position-independent scoring matrices" by Gutierres et al, 2020